import axios, { AxiosInstance } from "axios"

export const DEFAULT_BASE_URL = process.env.API_BASE_URL || ""

const instance: AxiosInstance = axios.create({
	baseURL: DEFAULT_BASE_URL,
	timeout: 10000,
	headers: {
		"Content-Type": "application/json",
	},
})

export default instance
