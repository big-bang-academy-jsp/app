"use client"

import { useCallback, useEffect, useState } from "react"
import styles from "./page.module.scss"
import axios from "../services/axios"
import { UserProps } from "../types"
import { Card, LinearProgress, Typography } from "@mui/material"
import Counter from "./components/Counter"

export default function Home() {
	const [users, setUsers] = useState<UserProps[]>([])
	const [loading, setLoading] = useState<boolean>(false)

	const fetchData = useCallback(async () => {
		try {
			setLoading(true)
			const res = await axios.get(
				`${process.env.NEXT_PUBLIC_BASE_URL}/api/users`
			)
			const data = res?.data?.users
			if (Array.isArray(data)) {
				setUsers(data)
			}
		} catch (error) {
			console.log("error")
			console.log(error)
		} finally {
			setLoading(false)
		}
	}, [])

	useEffect(() => {
		fetchData()
	}, [])

	return (
		<main className={styles.main}>
			{loading && <LinearProgress />}
			<div className={styles.container}>
				<Typography variant="h5">User List</Typography>
				<div className={styles.list}>
					{users?.map((obj) => (
						<Card key={obj.id} className={styles.card}>
							{obj.name}
						</Card>
					))}
				</div>
			</div>
			<div className={styles.container}>
				<Typography variant="h5">Counter</Typography>
				<Counter />
			</div>
		</main>
	)
}
