import { Button, Card } from "@mui/material"
import React, { useCallback, useEffect, useState } from "react"
import styles from "./Counter.module.scss"

interface IProps {
	defaultCount?: number
	step?: number // number per increment/decrement
	countOnChange?: (count: number) => void
}

const Counter = ({ defaultCount = 0, step = 1, countOnChange }: IProps) => {
	const [count, setCount] = useState<number>(defaultCount)

	const increase = useCallback(() => {
		setCount((prev) => prev + step)
	}, [step])

	const decrease = useCallback(() => {
		setCount((prev) => prev - step)
	}, [step])

	useEffect(() => {
		// Allow parent to use the count value
		countOnChange && countOnChange(count)
	}, [count, countOnChange])

	return (
		<Card className={styles.root}>
			<Button onClick={decrease}>Decrement</Button>
			<p>{count}</p>
			<Button onClick={increase}>Increment</Button>
		</Card>
	)
}

export default Counter
